﻿using Slapper;
using System.Data;
using TestConsole.Entities;

namespace TestConsole
{
    public class AlfredDb : Database
    {
        public AlfredDb(IDbConnection dbConnection) : base(dbConnection) { }

        public Table<Dummy> Dummies { get; set; }
        public Table<Child> Children { get; set; }
    }
}
