﻿using System;
using System.Threading.Tasks;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            using (var connection = new MySql.Data.MySqlClient.MySqlConnection("server=localhost;user=root;database=<your-db>;port=3306;password=<your password>;Allow User Variables=true;"))
            {
                var db = new AlfredDb(connection);

                foreach (var dummy in await db.Dummies.AllAsync())
                {
                    Console.WriteLine($"Id: {dummy.Id}\tName: {dummy.Name}");
                }
            }
        }

    }
}
