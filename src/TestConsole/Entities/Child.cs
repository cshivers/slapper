﻿namespace TestConsole.Entities
{
    public class Child
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
