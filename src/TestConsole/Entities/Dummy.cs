﻿namespace TestConsole.Entities
{
    public class Dummy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual Child Child { get; set; }
    }
}
