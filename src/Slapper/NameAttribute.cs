﻿using System;

namespace Slapper
{
    /// <summary>
    /// Currently used to designate table names that do not match the Property or Type name. 
    /// </summary>
    public class NameAttribute : Attribute
    {
        public string TableName { get; }
        public NameAttribute(string tableName)
        {
            TableName = tableName;
        }
    }
}
