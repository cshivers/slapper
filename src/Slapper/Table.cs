﻿using Dapper;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Slapper
{
    /// <summary>
    /// A generic class that generates simple CRUD sql queries and runs them against the underlying <see cref="IDbConnection"/>.
    /// </summary>
    /// <typeparam name="T">A POCO class that matches a database table.</typeparam>
    public class Table<T> where T : class
    {
        private readonly IDbConnection connection;

        /// <summary>
        /// The table name derived for SQL purposes.
        /// </summary>
        public string TableName { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbConnection">An <see cref="IDbConnection"/>. (currently only MySql databases are supported).</param>
        /// <param name="tableName">An optional paramenter to specify the corresponding table name in the database.</param>
        public Table(IDbConnection dbConnection, string tableName = null)
        {
            connection = dbConnection;
            TableName = tableName ?? typeof(T).Name;
        }

        /// <summary>
        /// Get all records from a table.
        /// </summary>
        /// <returns>A(n) <see cref="T"/> object with corresponding data from the database.</returns>
        public Task<IEnumerable<T>> AllAsync()
            => connection.QueryAsync<T>($"SELECT * FROM `{TableName}`");

        /// <summary>
        /// Get all records from a table matching the given constraints."/>
        /// </summary>
        /// <param name="where">A object with properties that match valid column constraints.</param>
        /// <returns>A(n) <see cref="T"/> object with corresponding data from the database.</returns>
        public Task<IEnumerable<T>> AllAsync(dynamic where = null)
        {
            if (where == null)
                return AllAsync();

            string sql = $"SELECT * FROM `{TableName}`";

            List<string> paramNames = GetParamNames((object)where);
            if (!paramNames.Any())
                return AllAsync();

            string w = string.Join(" AND ", paramNames.Select(p => $"`{p}` = @{p}"));

            return connection.QueryAsync<T>($"SELECT * FROM `{TableName}` WHERE {w}", (object)where);
        }

        /// <summary>
        /// Delete a record from a table.
        /// </summary>
        /// <param name="entity">A <see cref="T"/> that will be deleted.</param>
        /// <returns><see cref="bool"/> indicating success.</returns>
        public async Task<bool> DeleteAsync(T entity)
            => (await connection.ExecuteAsync("DELETE FROM " + TableName + " WHERE Id = @id", (object)GetId(entity)).ConfigureAwait(false)) > 0;

        /// <summary>
        /// Delete a record from a table.
        /// </summary>
        /// <param name="id">The Id value associated with the <see cref="T"/> entity for deletion.</param>
        /// <returns><see cref="bool"/> indicating success.</returns>
        public async Task<bool> DeleteAsync(dynamic id)
            => (await connection.ExecuteAsync("DELETE FROM " + TableName + " WHERE Id = @id", (object)id).ConfigureAwait(false)) > 0;

        /// <summary>
        /// Gets the first row from the table (order determined by the database provider).
        /// </summary>
        /// <returns><see cref="T"/> object with corresponding data from the database.</returns>
        public Task<T> FirstAsync()
            => connection.QueryFirstOrDefaultAsync<T>($"SELECT * FROM `{TableName}` LIMIT 1");

        /// <summary>
        /// Gets the first row from the table (order determined by the database provider).
        /// </summary>
        /// <param name="where">A object with properties that match valid column constraints.</param>
        /// <returns><see cref="T"/> object with corresponding data from the database.</returns>
        public Task<T> FirstAsync(dynamic where = null)
        {
            if (where == null)
                return FirstAsync();

            List<string> paramNames = GetParamNames((object)where);
            if (!paramNames.Any())
                return FirstAsync();

            string w = string.Join(" AND ", paramNames.Select((string p) => $"`{p}` = @{p}"));
            return connection.QueryFirstOrDefaultAsync<T>($"SELECT * FROM `{TableName}` WHERE {w} LIMIT 1", (object)where);
        }

        /// <summary>
        /// Gets a record with a particular Id from the database.
        /// </summary>
        /// <param name="id">The Id value associated with the <see cref="T"/> entity.</param>
        /// <returns><see cref="T"/> object with corresponding data from the database.</returns>
        public Task<T> GetAsync(dynamic id)
            => connection.QueryFirstOrDefaultAsync<T>($"SELECT * FROM `TableName` WHERE Id = @id", new { id });

        /// <summary>
        /// Insert a record into the table.
        /// </summary>
        /// <param name="data">The object containing data that will be inserted into the table.</param>
        /// <param name="removeId">Indicator to remove Id from the parameters for auto-incrementing columns.</param>
        /// <returns>The Id associated with the inserted row.</returns>
        public async Task<dynamic> InsertAsync(dynamic data, bool removeId = true)
        {
            List<string> paramNames = GetParamNames((object)data);

            // auto incremeting IDs will be handled by the db
            // we need to remove them before inserting
            if (removeId)
                paramNames.Remove("Id");

            string cols = string.Join("`,`", paramNames);
            string cols_params = string.Join(",", paramNames.Select((string p) => "@" + p));
            string sql = $"INSERT INTO `{TableName}` (`{cols}`) VALUES ({cols_params});";

            // if we have an auto incrementing ID, then we want to capture the id
            if (removeId) sql += "SELECT LAST_INSERT_ID() AS Id;";

            // TODO: test this w/ different types
            dynamic result;
            if (removeId)
                result = (await connection.QueryFirstAsync<dynamic>(sql, (object)data));
            else
                result = (int)(await connection.ExecuteAsync(sql, (object)data));

            if (!removeId)
                result.Id = data.Id; // TODO: determine fail points


            return result.Id;
        }

        /// <summary>
        /// Update a record in the table.
        /// </summary>
        /// <param name="data">The object containing data that will be inserted into the table.</param>
        /// <returns><see cref="int"/> with the number of affected rows.</returns>
        public Task<int> UpdateAsync(dynamic data)
        {
            List<string> paramNames = GetParamNames((object)data);

            var builder = new StringBuilder();
            builder.Append("UPDATE ").Append(TableName).Append(" SET ");
            builder.AppendLine(string.Join(",", paramNames.Where(n => n != "Id").Select(p => p + "= @" + p)));
            builder.Append("WHERE Id = @Id");

            var parameters = new DynamicParameters(data);
            parameters.Add("Id", data.Id);

            return connection.ExecuteAsync(builder.ToString(), parameters);
        }

        // cache to prevent deriving the params for the same type multiple times
        private static readonly ConcurrentDictionary<Type, List<string>> paramNameCache = new ConcurrentDictionary<Type, List<string>>();
        static List<string> GetParamNames(object o)
        {
            if (o is DynamicParameters parameters)
                return parameters.ParameterNames.ToList();

            if (o is ExpandoObject expando)
                return (expando as IDictionary<string, object>)?.Keys.ToList();

            if (!paramNameCache.TryGetValue(o.GetType(), out List<string> paramNames))
            {
                paramNames = new List<string>();
                foreach (var prop in o.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(p => p.GetGetMethod(false) != null && !p.GetGetMethod(false).IsVirtual))
                    paramNames.Add(prop.Name);

                paramNameCache[o.GetType()] = paramNames;
            }
            return paramNames;
        }
        private dynamic GetId(T entity)
        {
            var property =
                entity.GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .FirstOrDefault(
                    p => p.GetGetMethod(false) != null
                    && p.Name.Equals("Id", StringComparison.OrdinalIgnoreCase)
                );

            if (property == null) throw new ArgumentException("Entities must implement an Id property.", nameof(entity));
            return property.GetValue(entity);
        }
    }
}
