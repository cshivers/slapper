﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Slapper
{
    /// <summary>
    /// This class attempts to add simple CRUD actions to standard POCO classes
    /// </summary>
    public abstract class Database
    {
        private readonly IDbConnection connection;

        /// <summary>
        /// Abstract implementation that instantiates the <see cref="Table{T}"/> properties in the Database.
        /// </summary>
        /// <param name="dbConnection">An <see cref="IDbConnection"/>. (currently only MySql databases are supported).</param>
        protected Database(IDbConnection dbConnection)
        {
            connection = dbConnection;

            InitializeTables();
        }

        /// <summary>
        /// Initialize <see cref="Table{T}" /> properties to allow access to CRUD methods.
        /// </summary>
        private void InitializeTables()
        {
            var tables =
                GetType().GetRuntimeProperties()
                .Where(
                    p => !p.IsStatic()
                        && !p.GetIndexParameters().Any()
                        && p.DeclaringType != typeof(Database)
                        && p.PropertyType.GetTypeInfo().IsGenericType
                        && p.PropertyType.GetGenericTypeDefinition() == typeof(Table<>))
                .OrderBy(p => p.Name)
                .ToArray();

            // foreach table
            // pass in the connection
            // check for table name attribute
            // if it has one, pass it in as "tableName"
            foreach (var table in tables)
            {
                var genericType =
                    table
                    .PropertyType
                    .GetGenericTypeDefinition()
                    .MakeGenericType(
                        table.PropertyType.GetGenericArguments()
                    );

                // check for attributes at the Database Property level (priority)
                var attribute = table.GetCustomAttribute<NameAttribute>();

                // if there was none, check the class
                if (attribute == null)
                    table.PropertyType.GetGenericArguments()[0].GetCustomAttribute<NameAttribute>();

                // still null? default to the property name
                if (attribute == null)
                    attribute = new NameAttribute(table.Name);

                // create the Table<> instance and pass in the appropriate arguments
                table.SetValue(this, Activator.CreateInstance(genericType, connection, attribute?.TableName));
            }
        }
    }
}
